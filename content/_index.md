---
title: 'Homepage'
meta_title: 'IIMK Kommunity'
description: "IIM-K Community Domain"
intro_image: "images/pics/banner.jpg"
intro_image_absolute: true
intro_image_hide_on_mobile: false
---

# IIM-K Community Domain.

This domain is expected to be used as a base for the community activities of IIM Kozhikode.
