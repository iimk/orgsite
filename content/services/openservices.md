---
title: "Hosting Services for Community"
date: 2018-11-28T15:14:39+10:00
featured: true
draft: false
weight: 2
---

Hosting Open source services like matrix (slack alternative), forums, surveys, meetups, job postings boards for the benefit of current students and alumni of the Institute.

### Currently most of these activities are decentralised and limited to the small groups on various social media platforms. An institute wide service would help us retain control of our data and archive it as a knowledge base for future.
