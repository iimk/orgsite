---
title: "Sub Domains for Activities"
date: 2018-11-28T15:15:26+10:00
featured: true
draft: false
weight: 3
---

Subletting of sub domains for various community initiatives like interest groups, batch activities etc along with short term needs like events and online management games.


### We however do not have plans to provide hosting space for the sub-domains.
### We are in the process of finalizing the process for sub-domain request and distribution. Once finalised one can request subdomains like fitness.iimk.org.in or pgp12.iimk.org.in or markstrat.iimk.org.in etc.
We will reach out once the process is finalized
